# jwchat 微聊

> 基于 electron-vue 的仿微信客户端

 **先上一张对比图😀** 

![输入图片说明](https://images.gitee.com/uploads/images/2020/0812/172032_5147e406_4928216.png "屏幕截图.png")


### 安装/部署

``` bash
# 安装依赖
npm install

# 本地运行
npm run dev

# 编译打包成exe
npm run build

```
### 登录界面
![登录界面](https://images.gitee.com/uploads/images/2020/0812/165009_379e3728_4928216.gif "login.gif")

###  聊天界面

> 点击`登录`进入聊天界面

![聊天界面](https://images.gitee.com/uploads/images/2020/0812/165526_7deb04e1_4928216.gif "chat.gif")


#### 发送消息

 **支持图文混合，支持剪贴板 `ctrl + c` 直接粘贴图片** 

![发送消息 图文混合](https://images.gitee.com/uploads/images/2020/0812/170004_b1b4703f_4928216.png "屏幕截图.png")

 **发送表情** 

![发送表情](https://images.gitee.com/uploads/images/2020/0812/165709_0f1eda75_4928216.png "屏幕截图.png")

#### 发送文件

![发送文件](https://images.gitee.com/uploads/images/2020/0812/170129_e2de2951_4928216.png "屏幕截图.png")

#### 输入窗口大小拖拽

![输入窗口大小拖拽](https://images.gitee.com/uploads/images/2020/0812/170251_01b8c5d9_4928216.png "屏幕截图.png")

### 联系人界面

![联系人界面](https://images.gitee.com/uploads/images/2020/0812/170610_361f4ae6_4928216.png "屏幕截图.png")

 **添加好友** 

![添加好友](https://images.gitee.com/uploads/images/2020/0814/143702_2edf6627_4928216.png "屏幕截图.png")

 **公众号**

![公众号](https://images.gitee.com/uploads/images/2020/0814/143741_f49f1d43_4928216.png "屏幕截图.png") 

 **好友名片** 

![好友名片](https://images.gitee.com/uploads/images/2020/0814/143835_dab5d858_4928216.png "屏幕截图.png")


### 左侧菜单

![左侧菜单栏](https://images.gitee.com/uploads/images/2020/0812/170811_deadccff_4928216.gif "bar.gif")

### 顶部工具栏

![顶部工具栏](https://images.gitee.com/uploads/images/2020/0812/171029_93618691_4928216.png "屏幕截图.png")

![顶部工具栏](https://images.gitee.com/uploads/images/2020/0812/171229_02a3330f_4928216.png "屏幕截图.png")
